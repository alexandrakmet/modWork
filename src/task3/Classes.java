package task3;

/**
 * Created by Alexandra on 29.10.2018.
 */
public class Classes {
    public static int S = 0;


    //Base1 S:=2S+N-3, деструктор
    // Base2 S:=S/2-N, деструктор Alpha S:=S-N+3, деструктор Beta S:=S+N, деструктор Gamma S:=S-N,
    // деструктор Delta S:=S+3N-2.
    public static abstract class Base1 implements AutoCloseable {
        protected int N;
        protected static int NextBase1N = 1;

        protected Base1 B1;
        protected Base2 B2;

        protected Base1() {
            N = NextBase1N++;
        }

        @Override
        public void close() {
            S = 2 * S + N - 3;
            System.out.println("base1  s = " + S);
        }

        public int change(int s) {
            s = 2 * s + N - 3;
            return s;
        }

    }

    public abstract static class Base2 implements AutoCloseable {
        protected int N;
        protected static int NextBase2N = 1;

        protected Base1 B1;
        protected Base2 B2;

        protected Base2() {
            N = NextBase2N++;
        }

        @Override
        public void close() {
            S = S / 2 - N;
            System.out.println("base2  s = " + S);
        }


        public int change(int s) {
            s = s / 2 - N;
            return s;
        }

    }


    public static class Alpha extends Base1 implements AutoCloseable {
        protected static int NextAlphaN = 1;

        public Alpha(Boolean createB) {
            N = NextAlphaN++;

            if (createB) {
                B1 = new Alpha(false);
                B2 = new Delta(false);
            }
        }


        @Override
        public void close() {
            if (B1 != null && B2 != null) {
                S = 2 * S + this.N - 3;
                B1.close();
                B2.close();
                System.out.println("closing base1  S = " + S);
            } else {
                S = S - N + 3;
                System.out.println("closing base1  S = " + S);
            }

        }


        @Override
        public int change(int s) {
            if (B1 != null && B2 != null) {
                s = 2 * s + B1.N - 3;
                s = s / 2 - B2.N;
                s = 2 * s + this.N - 3;
                System.out.println("deleting base1  s = " + s);
            } else {
                s = s - N + 3;
                System.out.println("deleting alpha  s = " + s);
            }
            return s;
        }

    }

    public static class Beta extends Base1 {
        protected static int NextBetaN = 1;

        public Beta(Boolean createB) {
            N = NextBetaN++;

            if (createB) {
                B1 = new Beta(false);
                B2 = new Gamma(false);
            }
        }

        @Override
        public void close() {
            if (B1 != null && B2 != null) {
                S = 2 * S + this.N - 3;
                B1.close();
                B2.close();
                System.out.println("closing base1  S = " + S);
            } else {
                S = S + N;
                System.out.println("closing beta  S = " + S);
            }
        }


        public int change(int s) {
            if (B1 != null && B2 != null) {
                s = 2 * s + B1.N - 3;
                s = s / 2 - B2.N;
                s = 2 * s + this.N - 3;
                System.out.println("deleting base1  s = " + s);
            } else {
                s = s + N;
                System.out.println("deleting beta  s = " + s);
            }
            return s;
        }
    }


    public static class Delta extends Base2 {
        private static int NextDeltaN = 1;

        public Delta(Boolean createB) {
            N = NextDeltaN++;

            if (createB) {
                B1 = new Alpha(false);
                B2 = new Delta(false);
            }
        }

        @Override
        public void close() {
            if (B1 != null && B2 != null) {
                B1.close();
                B2.close();
                S = S / 2 - this.N;
                System.out.println("closing base1  S = " + S);
            } else {
                S = S + 3 * N - 2;
                System.out.println("closing delta  S = " + S);
            }
        }


        public int change(int s) {
            if (B1 != null && B2 != null) {
                s = 2 * s + B1.N - 3;
                s = s / 2 - B2.N;
                s = s / 2 - this.N;
                System.out.println("deleting base2  s = " + s);
            } else {
                s = s + 3 * N - 2;
                System.out.println("deleting delta  s = " + s);
            }

            return s;
        }

    }

    public static class Gamma extends Base2 {
        private static int NextGammaN = 1;

        public Gamma(Boolean createB) {
            N = NextGammaN++;

            if (createB) {
                B1 = new Beta(false);
                B2 = new Gamma(false);
            }
        }

        @Override
        public void close() {
            if (B1 != null && B2 != null) {
                B1.close();
                B2.close();
                S = S / 2 - this.N;
                System.out.println("closing base1  S = " + S);
            } else {
                S = S - N;
                System.out.println("closing gamma  S = " + S);
            }

        }

        public int change(int s) {
            if (B1 != null && B2 != null) {
                s = 2 * s + B1.N - 3;
                s = s / 2 - B2.N;
                s = s / 2 - this.N;
                System.out.println("deleting base2  s = " + s);
            } else {
                s = s - N;
                System.out.println("deleting gamma  s = " + s);
            }

            return s;
        }
    }

}

