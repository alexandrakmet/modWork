package task3;

import java.util.LinkedList;

/**
 * Created by Alexandra on 28.10.2018.
 */


public class Runner {

    static int tempS = Classes.S;
    static Object objects[];

    public static void demo1(){
        try(Classes.Alpha alpha = new Classes.Alpha(false);
            Classes.Base1 base1 = new Classes.Alpha(true);
            Classes.Delta delta = new Classes.Delta(false);
            Classes.Gamma gamma =  new Classes.Gamma(false);
            Classes.Alpha alpha1 = new Classes.Alpha(false)){
            System.out.println("alpha, N = " + alpha.N);
            System.out.println("base1, N = " + base1.N);
            System.out.println("delta, N = " + delta.N);
            System.out.println("gamma, N = " + gamma.N);
            System.out.println("alpha, N = " + alpha1.N);

        }
        System.out.println(Classes.S);
    }


    public static void demo2() {
        Classes.Base1 obj1 = new Classes.Alpha(true);
        Classes.Base2 obj2 = new Classes.Delta(true);
        Classes.Alpha obj3 = new Classes.Alpha(false);
        Classes.Beta obj4 = new Classes.Beta(false);
        Classes.Delta obj5 = new Classes.Delta(false);
        Classes.Gamma obj6 = new Classes.Gamma(false);

        LinkedList list = new LinkedList();
        list.add(obj1);
        list.add(obj2);
        list.add(obj3);
        list.add(obj4);
        list.add(obj5);
        list.add(obj6);

        System.out.println("if you delete elements in this sequence S will be " + change(tempS, list) + "\n");
    }

    public static int change(int s, LinkedList list) {
        for (Object obj : list) {
            if (obj.getClass() == Classes.Alpha.class) {
                Classes.Alpha a = (Classes.Alpha) obj;
                s = a.change(s);
            }
            if (obj.getClass() == Classes.Beta.class) {
                Classes.Beta b = (Classes.Beta) obj;
                s = b.change(s);
            }
            if (obj.getClass() == Classes.Delta.class) {
                Classes.Delta d = (Classes.Delta) obj;
                s = d.change(s);
            }
            if (obj.getClass() == Classes.Gamma.class) {
                Classes.Gamma g = (Classes.Gamma) obj;
                s = g.change(s);
            }
        }

        return s;
    }


    public static void demo3() {

        Classes.Base1 obj1 = new Classes.Alpha(true);
        Classes.Base2 obj2 = new Classes.Delta(true);
        Classes.Alpha obj3 = new Classes.Alpha(false);
        Classes.Beta  obj4 = new Classes.Beta (false);
        Classes.Delta obj5 = new Classes.Delta(false);
        Classes.Gamma obj6 = new Classes.Gamma(false);

        LinkedList list = new LinkedList();
        list.add(obj1);
        list.add(obj2);
        list.add(obj3);
        list.add(obj4);
        list.add(obj5);
        list.add(obj6);

        objects = list.toArray();

        permutation(0);

        System.out.println("\n\n");
    }

    public static int change(Object obj) {
        if (obj.getClass() == Classes.Alpha.class) {
            Classes.Alpha a = (Classes.Alpha) obj;
            tempS = a.change(tempS);
        }
        if (obj.getClass() == Classes.Beta.class) {
            Classes.Beta b = (Classes.Beta) obj;
            tempS = b.change(tempS);
        }
        if (obj.getClass() == Classes.Delta.class) {
            Classes.Delta d = (Classes.Delta) obj;
            tempS = d.change(tempS);
        }
        if (obj.getClass() == Classes.Gamma.class) {
            Classes.Gamma g = (Classes.Gamma) obj;
            tempS = g.change(tempS);
        }

        return tempS;
    }

    public static void permutation(int k) {

        int N = objects.length;
        if (k == N) {
            for (int i = 0; i < N; i++)
                tempS = change(objects[i]);
            System.out.println("if you delete elements in this sequence S will be " + tempS + "\n");

            tempS = Classes.S;
            System.out.println("now S = " + tempS);
        } else {
            for (int j = k; j < N; j++) {
                Object temp = objects[k];
                objects[k] = objects[j];
                objects[j] = temp;

                permutation(k + 1);

                temp = objects[k];
                objects[k] = objects[j];
                objects[j] = temp;
            }
        }
    }


    public static void main(String[] args) {
        demo1();
        demo2();
        demo3();
    }

}


