package task2;

import java.util.Arrays;
import java.util.LinkedList;


/**
 * Created by Alexandra on 27.10.2018.
 */
public class Function {

    static String error = "sorry please try later";

    static String description = "Реалізувати функцію f(x), що повертає текстове значення за наступними правилами:\n " +
            "1. Для натуральних (цілих додатніх) чисел n, f(n) = n 2 mod 103 англійською мовою\n" +
            "2. Для від’ємних цілих чисел n, f(n) = (n 5 +n 3 ) mod 203 англійською мовою\n" +
            "3. Для дійсних чисел d, f(d) = sin(d+303) перші дві цифри після коми англійською мовою\n" +
            "4. Для текстових рядків s, f(s) = слова з s у зворотньому порядку\n" +
            "5. Для пари p=(a,b), f(p) = по черзі по одному слову з f(b) та f(a)\n" +
            "6. Для списку v=[v 1 ,…,v k ], f(v) = унікальні слова з усіх f(v i ), у алфавітному порядку\n" +
            "7. Для всіх інших значень f(x) = “sorry please try again”*\n\n";

    //1, 2
    String func(int n) {
        int f;
        if (n > 0) {
            f = (n * n) % 103;
        } else f = (int) (Math.pow(n, 5) + Math.pow(n, 3)) % 103;
        return convert(f);
    }

    //3
    String func(double d) {
        double f = Math.sin(d + 303);
        f = (double) Math.round(f * 100d) / 100d;
        return f == 1 ? convert(f) : convert(f % 1);
    }

    //4
    String func(String sentence) {
        String sentenceRev = new StringBuilder(sentence).reverse().toString();
        String res = "";

        for (String part : sentenceRev.split(" ")) {
            res += new StringBuilder(part).reverse().toString();
            res += " ";
        }
        return res;
    }

    //5
    String func(Object object1, Object object2) {
        String res = "";
        String res1 = func(object1);
        String res2 = func(object2);

        if (object1.getClass().equals(Integer.class)) res1 = func((int) object1);
        if (object1.getClass().equals(Double.class) || object1.getClass().equals(Float.class))
            res1 = func((double) object1);
        if (object1.getClass().equals(String.class)) res1 = func((String) object1);

        if (object2.getClass().equals(Integer.class)) res2 = func((int) object2);
        if (object2.getClass().equals(Double.class) || object2.getClass().equals(Float.class))
            res2 = func((double) object2);
        if (object2.getClass().equals(String.class)) res2 = func((String) object2);


        if (!(res1.equals(error) || res2.equals(error))) {
            String[] parts1 = res1.split(" ");
            String[] parts2 = res2.split(" ");

            int min = Math.min(parts1.length, parts2.length);
            for (int i = 0; i < min; i++) {
                res += parts2[i];
                res += " ";
                res += parts1[i];
                res += " ";
            }
            if (parts1.length == min)
                for (int i = min; i < parts2.length; i++) {
                    res += parts2[i];
                    res += " ";
                }
            else for (int i = min; i < parts1.length; i++) {
                res += parts1[i];
                res += " ";
            }
        } else res = error;
        return res;
    }

    //6
    String func(LinkedList linkedList) {
        String res = "";
        String tempRes = "";
        Object list[] = linkedList.toArray();
        String parts[][] = new String[list.length][];

        for (int i = 0; i < list.length; i++) {
            if (list[i].getClass().equals(Integer.class)) parts[i] = func((int) list[i]).split(" ");
            if (list[i].getClass().equals(Double.class) || list[i].getClass().equals(Float.class))
                parts[i] = func((double) list[i]).split(" ");
            if (list[i].getClass().equals(String.class)) parts[i] = func((String) list[i]).split(" ");
        }

        for (String str[] : parts) {
            for (String s : str) {
                tempRes += s;
                tempRes += " ";
            }
        }


        for (String str[] : parts) {
            for (String s : str) {
                String temp = s;
                tempRes = tempRes.replaceFirst(temp, "");
                if (!tempRes.contains(temp)) {
                    res += temp;
                    res += " ";
                }
            }
        }

        String result[] = res.split(" ");
        Arrays.sort(result);
        res = "";

        for (String str : result) {
            res += str;
            res += " ";
        }


        return res;
    }


    //7
    String func(Object param) {
        return error;
    }


    private static final String[] units = {"", "One", "Two", "Three", "Four",
            "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve",
            "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen",
            "Eighteen", "Nineteen"};

    private static final String[] tens = {
            "",        // 0
            "",        // 1
            "Twenty",    // 2
            "Thirty",    // 3
            "Forty",    // 4
            "Fifty",    // 5
            "Sixty",    // 6
            "Seventy",    // 7
            "Eighty",    // 8
            "Ninety"    // 9
    };

    private static String convert(final double n) {
        if (n < 0) {
            return "Minus " + convert(-n);
        }


        if (n > 0 && n < 1) {
            return "Zero Point " + convert(n * 100);
        }

        if (n < 20) {
            return units[(int) n];
        }

        if (n < 100) {
            return tens[(int) n / 10] + ((n % 10 != 0) ? " " : "") + units[(int) n % 10];
        }

        if (n < 1000) {
            return units[(int) n / 100] + " Hundred" + ((n % 100 != 0) ? " " : "") + convert(n % 100);
        }

       /* if (n < 100000) {
            return convert(n / 1000) + " Thousand" + ((n % 10000 != 0) ? " " : "") + convert(n % 1000);
        }

        if (n < 10000000) {
            return convert(n / 100000) + " Lakh" + ((n % 100000 != 0) ? " " : "") + convert(n % 100000);
        }
        */

        return convert(n / 10000000) + " Crore" + ((n % 10000000 != 0) ? " " : "") + convert(n % 10000000);
    }
}


