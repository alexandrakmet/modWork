package task2;

import java.util.LinkedList;

/**
 * Created by Alexandra on 27.10.2018.
 */
public class Runner {


    public static void main(String[] args) {
        Function f = new Function();
        int n = 15, minN = -10;
        double d = 17.7;
        String s1 = "Have A Nice Day";

        LinkedList l = new LinkedList();
        l.add(n);
        l.add(minN);
        l.add(d);
        l.add(s1);
        l.add("Zero day");


        System.out.println(Function.description);

        System.out.println("f(" + n + ") = " + f.func(n));
        System.out.println("f(" + minN + ") = " + f.func(minN));
        System.out.println("f(" + d + ") =  " + f.func(d));
        System.out.println("f(" + s1 + ") = " + f.func(s1));
        System.out.println("f(" + s1 + ", " + d + ") = " + f.func(s1, d));
        System.out.println("f(" + n + ", " + minN + ", " + d + ", " + s1 + ") = " + f.func(l));


    }


}
