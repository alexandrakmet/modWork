package task1;

import java.util.LinkedList;

/**
 * Created by Alexandra on 28.10.2018.
 */
public class Storage {

    int numb;
    int x,y;
   // int load, unload;
    boolean ifunload;
    Storage storageN[];
    int dist[];
    LinkedList<Cargo> cargos;

    //Storage(int numb, int x, int y, )

    public double distance(Storage stor){
        return Math.sqrt(Math.pow(this.x-stor.x,2)+Math.pow(this.y-stor.y,2));
    }

    public Cargo unloading(Vehicle v){
        Cargo cargo = cargos.pollLast();
        if(v.maxWeight-v.currentWeight>cargo.weight) return cargo;
        else return null;
    }

    public void loading(Vehicle v){
        Cargo cargo = v.cargo.pollLast();
        cargos.add(cargo);
    }
}
