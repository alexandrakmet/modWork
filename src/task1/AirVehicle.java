package task1;

/**
 * Created by Alexandra on 27.10.2018.
 */
public class AirVehicle extends Vehicle {

    public Storage start, end;

    AirVehicle(int name, int maxW,int  maxV, int sp,  double lT, double unlT){
        this.name = name;
        this.maxWeight = maxW;
        this.maxVolume = maxV;
        this.speed = sp;
        this.loadT = lT;
        this.unloadT = unlT;
    }


}
