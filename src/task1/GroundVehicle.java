package task1;

/**
 * Created by Alexandra on 27.10.2018.
 */
public class GroundVehicle extends Vehicle {

//    int maxWeight, maxVolume, speed;
//    int load, unload;
//    int currentWeight, currentVolume;
//    Cargo cargo;

    //   Storage storages[];

    GroundVehicle(int name, int maxW, int maxV, int sp, double lT, double unlT) {
        this.name = name;
        this.maxWeight = maxW;
        this.maxVolume = maxV;
        this.speed = sp;
        this.loadT = lT;
        this.unloadT = unlT;
    }

    public int route(Storage storages[]) {
        int timeInRoute = 0;
        System.out.println("Lorry numb."+name);
        for (int i = 0; i < storages.length; i++) {
            timeInRoute += storages[i].distance(storages[i + 1]) / speed;
            storages[i].loading(this);
            timeInRoute+=loadT;
            if (storages[i].ifunload) {
                cargo.add(storages[i].unloading(this));
                timeInRoute += unloadT;
            }
        }
        return timeInRoute;
    }

}
